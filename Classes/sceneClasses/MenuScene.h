#ifndef __MENUSCENE_SCENE_H__
#define __MENUSCENE_SCENE_H__

#include "cocos2d.h"

class MenuScene : public cocos2d::LayerGradient
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(MenuScene);

	cocos2d::Menu* createMenu();
	void PlayGame(cocos2d::Ref* pSender);
	void HighScores(cocos2d::Ref* pSender);
	void addEvent();
};

#endif // __MENUSCENE_SCENE_H__
