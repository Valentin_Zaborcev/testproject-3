#include "cocos2d.h"
#include "CloudGenerator.h"

#define RAND(a, b) cocos2d::RandomHelper::random_real(a, b);

using namespace cocos2d;

CloudGenerator* CloudGenerator::create() {
	auto cloudGenerator = new CloudGenerator();
	if (cloudGenerator->init()) {
		cloudGenerator->autorelease();
		return cloudGenerator;
	}
	return nullptr;
}

void CloudGenerator::generateWave(int cloudAmount) {
	auto vSize = Director::getInstance()->getVisibleSize();
	for (int i = 0; i < cloudAmount; i++) {
		auto posY = RAND(0.f, vSize.height);
		auto cloudPosition = Vec2(vSize.width * (i+1) / cloudAmount, posY);
		auto cloud = Cloud::create(cloudPosition);
		float speed = RAND(30.f, 70.f);
		cloud->setSpeed(speed);
		cloud->startMove();
		addChild(cloud);
		_clouds.pushBack(cloud);
	}
}