#ifndef __CLOUD_H__
#define __CLOUD_H__

#include "cocos2d.h"
#include "SimpleObject.h"

class Cloud : public SimpleObject {
protected:
	Cloud() {}
	Cloud(const Cloud&) {}

public:
	virtual ~Cloud();
	static Cloud* create(const cocos2d::Vec2& pos = cocos2d::Vec2::ZERO);
	void startMove() override;
	void removeItSelf();
};

#endif // __CLOUD_H__