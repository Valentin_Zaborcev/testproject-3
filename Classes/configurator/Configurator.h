#ifndef __CONFIGURATOR_H__
#define __CONFIGURATOR_H__

#include <string>
#include "cocos2d.h"

using std::string;

namespace config {
	class Configurator {
	private:
		Configurator();
		Configurator(const Configurator&);
		~Configurator();
		static Configurator* pInstance;
		int _points;

	public:
		cocos2d::Label* label;
		float animationTime;
		float cloudScale;
		float birdAverageHeight;
		float waveTimer;
		int birdsMaxAmount;
		int birdsStartAmount;
	public:
		static Configurator* getInstance() {
			if (!pInstance) {
				pInstance = new Configurator();
			}
			return pInstance;
		}
		bool initFromFile(const string& fName);
		void saveToFile(const string& fName);
		void initDefault();
		cocos2d::Label* initLabel();

		bool gotInTable(const string& fName);
		void setPoints(int points) { _points = points; }
		void addPoints(int points) { _points += points;  }
		int  getPoints() { return _points;  }
	};
}

#endif //__CONFIGURATOR_H__